﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public static int score;
	Text text;

	void Start()
	{
		text = GetComponent<Text> ();
		score += 0;
	}

	void Update()
	{
		text.text = "" + score;
	}

	public static void Addpoints(int pointstoadd)
	{
		score += pointstoadd;
	}

	public static void Reset()
	{
		score = 0;
	}
}
