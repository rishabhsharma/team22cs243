﻿using UnityEngine;
using System.Collections;

public interface IEnemyState
{
	void Execute ();                                //these are public by default
	void Enter(Enemy enemy);
	void Exit ();
	void OnTriggerEnter(Collider2D other);
}
