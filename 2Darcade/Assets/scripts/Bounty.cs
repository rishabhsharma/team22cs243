﻿using UnityEngine;
using System.Collections;

public class Bounty : MonoBehaviour {
	public AudioSource Audio1;
	public int pointstoadd;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag=="Player") 
		{
			Score.Addpoints (pointstoadd);
			Destroy (gameObject); 
			Audio1.Play ();
		}
	}
}
