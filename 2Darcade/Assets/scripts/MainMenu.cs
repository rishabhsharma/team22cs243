﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	
	public string startLevel;

	public string levelSelect;

	public string level1Tag;

	public void NewGame()
	{
		GetComponent <AudioSource> ().Play ();
		PlayerPrefs.SetInt (level1Tag,1	);
		PlayerPrefs.SetInt ("PlayerLevelSelectPosition", 0);
		Score.Reset();
		SceneManager.LoadScene (startLevel);

	}

	public void LevelSelect()
	{
		GetComponent <AudioSource> ().Play ();
		PlayerPrefs.SetInt (level1Tag,1	);
		if (!PlayerPrefs.HasKey ("PlayerLevelSelectPosition")) 
		{
			PlayerPrefs.SetInt ("PlayerLevelSelectPosition", 0);
		}
		Score.Reset();
		SceneManager.LoadScene(levelSelect);
		SceneManager.LoadScene(levelSelect);
	}

	public void QuitGame()
	{
		GetComponent <AudioSource> ().Play ();
		Score.Reset() ;
		Application.Quit ();
	}
}
