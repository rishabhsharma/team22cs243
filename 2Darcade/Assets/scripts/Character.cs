﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Character : MonoBehaviour {          //abstarct class will prevent character script to b added to gameobject
	                                                //accesible within class and classes derived from that class
	[SerializeField]
	protected Transform KnifePos;                             
	[SerializeField]
	protected float movementSpeed;

	protected bool faceRight;

	[SerializeField]
	private GameObject knifePrefab;

	[SerializeField]
	protected int health;

	//public static int enemyHealth ;
	//public static int playerHealth ;

	[SerializeField]
	private EdgeCollider2D swordCollider;
	[SerializeField]
	private List<string> damageSources;

	public abstract bool IsDead { get; }      //charcter is not gonna return after death so only get

	public bool Attack { get; set;}
	public bool TakingDamage{ get; set; }
	public Animator MyAnimator{ get ; private set;}

	public EdgeCollider2D SwordCollider {
		get {
			return swordCollider;
		}
	}

	public virtual void Start () {
		faceRight = true;
		MyAnimator = GetComponent<Animator> ();
	}
	

	void Update () {
	
	}

	public abstract IEnumerator TakeDamage ();     //abstract-every class inherited has to use this fuction

	public abstract void Death ();

	public void changeDirection()
	{
		faceRight = !faceRight;
		transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y, 1);
	}

	public virtual void ThrowKinfe(int value){
		if (faceRight) 
		{
			GameObject temp=(GameObject)Instantiate (knifePrefab, KnifePos.position, Quaternion.Euler(new Vector3(0,0,-90)));
			temp.GetComponent<Knife> ().Initialize (Vector2.right);
		} 
		else 
		{
			GameObject temp=(GameObject)Instantiate (knifePrefab, KnifePos.position, Quaternion.Euler(new Vector3(0,0,90)));
			temp.GetComponent<Knife> ().Initialize (Vector2.left);
		}
	}

	public void MeleeAttack()
	{
		SwordCollider.enabled = true;
	}

	public virtual void OnTriggerEnter2D(Collider2D other)
	{
		if (damageSources.Contains(other.tag)) {
			StartCoroutine (TakeDamage ());
		}
	}
}
