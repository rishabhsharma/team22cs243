﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class healthbar : MonoBehaviour{


	private float fillAmount;

	[SerializeField]
	private float lerpspeed;

	[SerializeField]
	private Image content;

	[SerializeField]
	private Text valueText;

	[SerializeField]
	private Color fullcolor;

	[SerializeField]
	private Color lowcolor;

	[SerializeField]
	private bool lerpColors;

	public float MaxValue{ get; set;}

	public float Value
	{
		
		set
		{
			string[] tmp = valueText.text.Split(':');
			valueText.text = tmp [0] + ":" + value;
			fillAmount = Map (value, 0, MaxValue, 0, 1);	
		}

	}



	void Start()
	{
		if (lerpColors) 
		{
			content.color = fullcolor;		
		}


	}

	void Update()
	{
		HandleBar ();
	}

	private void HandleBar()
	{
		if (fillAmount != content.fillAmount) 
		{
			content.fillAmount = Mathf.Lerp(content.fillAmount,fillAmount, Time.deltaTime *lerpspeed);
		}
		if (lerpColors) 
		{
			content.color = Color.Lerp (lowcolor, fullcolor, fillAmount);
		}

	}

	private float Map(float value, float inMin, float inMax, float outMin, float outMax)
	{
		return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}
}