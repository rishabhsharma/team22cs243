﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public string levelSelect;
	public string mainMenu;

	public bool isPaused;

	public GameObject pauseMenuCanvas;

	public AudioSource Audiop1;
	public AudioSource Audiop2;
	// Update is called once per frame
	void Update () {
		if (isPaused) {
			pauseMenuCanvas.SetActive (true);
			Time.timeScale = 0f;
		} 
		else {
			pauseMenuCanvas.SetActive (false);
			Time.timeScale = 1f;
		}

		if(Input.GetKeyDown(KeyCode.Escape))
			{
			Audiop2.Play ();
				isPaused=!isPaused;
			}
	}

	public void Resume()
	{
		Audiop1.Play ();
		isPaused = false;
	}

	public void LevelSelect()
	{
		Audiop1.Play ();
		isPaused = false;
		Score.Reset ();
		SceneManager.LoadScene(levelSelect);
	}

	public void Quit()
	{
		Audiop1.Play ();
		Score.Reset();
		SceneManager.LoadScene (mainMenu);
	}
}
